const input = document.querySelector("#input");
const todo = document.querySelector("#toDo");
const addBtn = document.querySelector("#addBtn");
const select = document.querySelector("#select");

addBtn.addEventListener("click", addToDo);
select.addEventListener("click", filterToDo);

function addToDo(e) {
  e.preventDefault();

  if (input.value == "") {
    alert("please fill the input box");
  } else {
    const toDoItem = document.createElement("div");

    const newToDo = document.createElement("li");
    newToDo.innerText = input.value;
    toDoItem.appendChild(newToDo);

    const checked = document.createElement("button");
    checked.classList.add("checkBtn");
    checked.innerHTML = '<i class="fas fa-check"></i>';
    toDoItem.appendChild(checked);

    checked.addEventListener("click", (e) => {
      newToDo.classList.toggle("completed");
      toDoItem.classList.toggle("completed");
    });

    const delet = document.createElement("button");
    delet.classList.add("deleteBtn");
    delet.innerHTML = '<i class="fas fa-times"></i>';
    toDoItem.appendChild(delet);

    delet.addEventListener("click", (e) => {
      e.target.parentElement.remove();
    });

    todo.appendChild(toDoItem);

    input.value = "";
  }
}

function filterToDo(e) {
  const list = todo.childNodes;
  list.forEach((todo) => {
    switch (e.target.value) {
      case "all":
        todo.style.display = "flex";
        break;
      case "completed":
        if (todo.classList.contains("completed")) {
          todo.style.display = "flex";
        } else {
          todo.style.display = "none";
        }
        break;
      case "pending":
        if (!todo.classList.contains("completed")) {
          todo.style.display = "flex";
        } else {
          todo.style.display = "none";
        }
        break;
    }
  });
}
